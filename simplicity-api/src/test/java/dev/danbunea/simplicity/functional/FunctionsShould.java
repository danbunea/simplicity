package dev.danbunea.simplicity.functional;

import io.vavr.collection.HashMap;
import io.vavr.collection.Map;

import org.junit.Test;

import java.util.function.Function;

import static dev.danbunea.simplicity.functional.Functions.*;
import static org.assertj.core.api.Assertions.assertThat;


public class FunctionsShould {


    @Test
    public void
    execute_a_function() {
        //given
        Function<Map<String, String>, Map<String, String>> function = state -> state.put("message", "Hello Dan");
        //when
        Map<String, String> result = function.apply(HashMap.of("message", "no message"));
        //then
        assertThat(result).isEqualTo(HashMap.of("message", "Hello Dan"));
    }


    @Test
    public void
    execute_a_composed_function() {
        //given
        Function<Map<String, String>, Map<String, String>> functionOne = state -> state.put("message", "Dan");
        Function<Map<String, String>, Map<String, String>> functionTwo = state -> state.computeIfPresent("message", (k, name) -> "Hello " + name)._2();

        //when
        Function<Map<String, String>, Map<String, String>> composedFunction = compose2(functionOne, functionTwo);

        //then
        assertThat(composedFunction.apply(HashMap.of("message", ""))).isEqualTo(HashMap.of("message", "Hello Dan"));

    }

    @Test
    public void
    execute_a_composed_function_of_3() {
        //given
        Function<Map<String, String>, Map<String, String>> functionOne = state -> state.put("message", "Dan");
        Function<Map<String, String>, Map<String, String>> functionTwo = state -> state.computeIfPresent("message", (k, name) -> "Hello " + name)._2();
        Function<Map<String, String>, Map<String, String>> functionThree = state -> state.computeIfPresent("message", (k, message) -> message.toUpperCase())._2();

        //when
        Function<Map<String, String>, Map<String, String>> composedFunction = compose(functionOne, functionTwo, functionThree);

        //then
        assertThat(composedFunction.apply(HashMap.of("message", ""))).isEqualTo(HashMap.of("message", "HELLO DAN"));

    }

    @Test
    public void
    execute_a_pipe_of_functions() {
        //given

        //when
        Map<String, String> initialState = HashMap.of("message", "");
        Map<String, String> finalState = pipe(initialState,
                state -> state.put("message", "Dan"),
                state -> state.computeIfPresent("message", (k, name) -> "Hello " + name)._2(),
                state -> state.computeIfPresent("message", (k, message) -> String.valueOf(message).toUpperCase())._2()
        );

        //then
        assertThat(finalState).isEqualTo(HashMap.of("message", "HELLO DAN"));
    }

    @Test
    public void
    get_a_value_non_existing_value() {
        //given
        Map<String, Object> initialState = HashMap.empty();
        //when
        Object finalState = get("key").apply(initialState);
        //then
        assertThat(finalState).isNull();
    }

    @Test
    public void
    get_a_value() {
        //given
        Map<String, Object> initialState = HashMap.of("key", "value");
        //when
        Object finalState = get("key").apply(initialState);
        //then
        assertThat(finalState).isEqualTo("value");
    }







}