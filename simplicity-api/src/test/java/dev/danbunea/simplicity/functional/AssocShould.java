package dev.danbunea.simplicity.functional;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import org.junit.Test;

import static dev.danbunea.simplicity.functional.Functions.assoc;
import static dev.danbunea.simplicity.functional.Functions.assoc_in;
import static dev.danbunea.simplicity.functional.Functions.pipe;
import static org.assertj.core.api.Assertions.assertThat;

public class AssocShould {

    @Test
    public void
    assoc_a_value() {
        //given
        Map<String, String> initialState = HashMap.empty();
        //when
        Map<String, String> finalState = pipe(initialState,
                assoc("key", "value"));
        //then
        assertThat(finalState).isEqualTo(HashMap.of("key", "value"));
    }

    @Test
    public void
    override_a_value() {
        //given
        Map<String, String> initialState = HashMap.of("key", "value");
        //when
        Map<String, String> finalState = pipe(initialState,
                assoc("key", "another_value"));
        //then
        assertThat(finalState).isEqualTo(HashMap.of("key", "another_value"));
    }

    @Test
    public void
    assoc_in_a_value() {
        //given
        Map<String, Object> initialState = HashMap.of(
                "id", "o1",
                "person", HashMap.of(
                        "id", "o2",
                        "address", HashMap.of(
                                "id", "o3",
                                "street", "Numancia")));
        //when
        Map<String, Object> finalState = pipe(initialState,
                assoc_in(List.of("person", "address", "street"), "value"));
        //then
        assertThat(finalState).isEqualTo(HashMap.of(
                "id", "o1",
                "person", HashMap.of(
                        "id", "o2",
                        "address", HashMap.of(
                                "id", "o3",
                                "street", "value"))));
    }

    @Test
    public void
    assoc_in_a_missing_value() {
        //given
        Map<String, Object> initialState = HashMap.of(
                "id", "o1",
                "person", HashMap.of(
                        "id", "o2",
                        "address", HashMap.of(
                                "id", "o3"
                        )));
        //when
        Map<String, Object> finalState = pipe(initialState,
                assoc_in(List.of("person", "address", "street"), "value"));
        //then
        assertThat(finalState).isEqualTo(HashMap.of(
                "id", "o1",
                "person", HashMap.of(
                        "id", "o2",
                        "address", HashMap.of(
                                "id", "o3",
                                "street", "value"))));
    }

    @Test
    public void
    assoc_in_a_missing_object_value() {
        //given
        Map<String, Object> initialState = HashMap.of(
                "id", "o1",
                "person", HashMap.of(
                        "id", "o2"));
        //when
        Map<String, Object> finalState = pipe(initialState,
                assoc_in(List.of("person", "address", "street"), "value"));
        //then
        assertThat(finalState).isEqualTo(HashMap.of(
                "id", "o1",
                "person", HashMap.of(
                        "id", "o2",
                        "address", HashMap.of(
                                "street", "value"))));
    }

    @Test
    public void
    assoc_in_a_missing_path() {
        //given
        Map<String, Object> initialState = HashMap.empty();
        //when
        Map<String, Object> finalState = pipe(initialState,
                assoc_in(List.of("person", "address", "street"), "value"));
        //then
        assertThat(finalState).isEqualTo(HashMap.of(
                "person", HashMap.of(
                        "address", HashMap.of(
                                "street", "value"))));
    }

    @Test(expected = ClassCastException.class)
    public void
    fail_overriding_non_map_in_path() {
        //given
        Map<String, Object> initialState = HashMap.of("person","Dan");

        //when
        Map<String, Object> finalState = pipe(initialState,
                assoc_in(List.of("person", "address", "street"), "value"));
    }

}
