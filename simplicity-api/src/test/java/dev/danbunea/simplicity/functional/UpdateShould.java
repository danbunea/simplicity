package dev.danbunea.simplicity.functional;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import org.junit.Test;

import static dev.danbunea.simplicity.functional.Functions.*;
import static org.assertj.core.api.Assertions.assertThat;

public class UpdateShould {
    @Test
    public void
    update_an_exiting_value() {
        //given
        Map<String, String> initialState = HashMap.of("key", "value");
        //when
        Map<String, String> finalState = pipe(initialState,
                update("key", String::toUpperCase));
        //then
        assertThat(finalState).isEqualTo(HashMap.of("key", "VALUE"));
    }

    @Test
    public void
    ignore_on_update_a_missing_value() {
        //given
        Map<String, String> initialState = HashMap.empty();
        //when
        Map<String, String> finalState = pipe(initialState,
                update("key", String::toUpperCase));
        //then
        assertThat(finalState).isEqualTo(initialState);
    }

    @Test
    public void
    update_in_a_value() {
        //given
        Map<String, Object> initialState = HashMap.of(
                "id", "o1",
                "person", HashMap.of(
                        "id", "o2",
                        "address", HashMap.of(
                                "id", "o3",
                                "street", "Numancia")));
        //when
        Map<String, Object> finalState = pipe(initialState,
                update_in(List.of("person", "address", "street"), (street)->street.toString().toUpperCase()));
        //then
        assertThat(finalState).isEqualTo(HashMap.of(
                "id", "o1",
                "person", HashMap.of(
                        "id", "o2",
                        "address", HashMap.of(
                                "id", "o3",
                                "street", "NUMANCIA"))));
    }
    @Test
    public void
    update_in_a_list_value() {
        //given
        Map<String, Object> initialState = HashMap.of(
                "person", HashMap.of(
                        "grades", List.of(7,8,9)));
        //when
        Map<String, Object> finalState = pipe(initialState,
                update_in(List.of("person", "grades"), (grades)->((List)grades).map(grade->(int)grade+1)));
        //then
        assertThat(finalState).isEqualTo(HashMap.of(
                "person", HashMap.of(
                        "grades", List.of(8,9,10))));
    }
}
