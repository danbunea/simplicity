package dev.danbunea.simplicity.functional;


import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.HashMap;

import java.util.Objects;
import java.util.function.Function;

public class Functions {
    public static <K, V> Function<Map<K, V>, Map<K, V>> compose2(Function<Map<K, V>, Map<K, V>> f, Function<Map<K, V>, Map<K, V>> g) {
        return (state) -> g.apply(f.apply(state));
    }

    public static <K, V> Function<Map<K, V>, Map<K, V>> compose(Function<Map<K, V>, Map<K, V>>... functions) {
        return List.of(functions).reduce(Functions::compose2);
    }

    public static <K, V> Map<K, V> pipe(Map<K, V> initialState, Function<Map<K, V>, Map<K, V>>... functions) {
        return compose(functions).apply(initialState);
    }

    public static <K, V> Function<Map<K, V>, Map<K, V>> assoc(K key, V value) {
        return state -> state.put(key, value);
    }


    public static <K, V> Function<Map<K, V>, Map<K, V>> assoc_in(List<K> path, V value) {
        return apply_in(path, value, null);

    }

    private static <K, V> Function<Map<K, V>, Map<K, V>> apply_in(List<K> path, V value, Function<V, V> function) {
        return state -> {
            List<Tuple2<K, Map>> associations = List.empty();

            Map<K, V> currentMap = state;
            for (K key : path) {
                V newMap = Objects.requireNonNull(currentMap).get(key).getOrElse((V) null);
                associations = associations.append(new Tuple2(key, currentMap));

                if (newMap == null) {
                    currentMap = HashMap.empty();
                } else if (newMap instanceof Map) {
                    currentMap = (Map) newMap;

                } else if (!(newMap instanceof Map) && !key.equals(path.last())) {
                    throw new ClassCastException("At '" + key + "' in " + path + " it's not a Map but: " + newMap);
                } else {
                    currentMap = null;
                }
            }

            Map<K, V> replaced = null;
            for (Tuple2<K, Map> association : associations.reverse()) {

                if (replaced != null)
                    replaced = (Map<K, V>) assoc(association._1(), replaced).apply(association._2());
                else {
                    Function<Map<K, V>, Map<K, V>> func = function == null ? assoc(association._1(), value) : update(association._1(), function);
                    replaced = func.apply(association._2());
                }
            }

            return replaced;
        };
    }

    public static <K, V> Function<Map<K, V>, Map<K, V>> update(K key, Function<V, V> function) {
        return state -> state.computeIfPresent(key, (k, name) -> function.apply(name))._2();
    }

    public static <K, V> Function<Map<K, V>, Map<K, V>> update_in(List<K> path, Function<V, V> function) {
        return apply_in(path, null, function);
    }


    public static <K, V> Function<Map<K, V>, V> get(K key) {
        return state -> state.get(key).getOrElse((V) null);
    }
}
