#!/bin/sh
export MAVEN_OPTS="${MAVEN_OPTS} -Dorg.slf4j.simpleLogger.showDateTime=true"
export MAVEN_CLI_OPTS="${MAVEN_CLI_OPTS} --show-version -DinstallAtEnd=true -DdeployAtEnd=true"

mvn ${MAVEN_CLI_OPTS} clean verify
